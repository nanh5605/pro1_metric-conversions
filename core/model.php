<?php 

function get_fullname ($conn, $id){
    $stmt = $conn->prepare("SELECT * FROM user WHERE id = $id");
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function list_modules ($conn, $table){
    $stmt = $conn->prepare("SELECT * FROM $table ORDER BY id DESC");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function list_cate($conn){
    $stmt = $conn->prepare("SELECT * FROM unit_category");
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function create_unit_category ($conn, $data, &$errors){
    $stmt_check = $conn->prepare("SELECT * FROM unit_category WHERE name = :name");
    $stmt_check->bindParam(":name", $data['name'], PDO::PARAM_STR);
    $stmt_check->execute();

    $count = $stmt_check->rowCount();

    if ($count == 0){
        $stmt = $conn->prepare("INSERT INTO unit_category (name, slug, status) VALUES (:name, :slug, :status)");
        $stmt->bindParam(":name", $data['name'], PDO::PARAM_STR);
        $stmt->bindParam(":slug", $data['slug'], PDO::PARAM_STR);
        $stmt->bindParam(":status", $data['status'], PDO::PARAM_INT);
        $stmt->execute();

        header("location:index.php?p=create-unit_category");
        exit();
    } else {
        $errors = "Data is exist";
    }
}

function get_one_modules ($conn, $id, $modules){
    $stmt = $conn->prepare("SELECT * FROM $modules WHERE id =:id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function edit_unit_category ($conn, $data){
    
    $stmt = $conn->prepare("UPDATE unit_category SET name=:name,slug=:slug,status=:status WHERE id = :id");
    
    $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt->bindParam(":slug", $data["slug"], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
    $stmt->bindParam(":id", $data["id"], PDO::PARAM_INT);
    $stmt->execute();

    header("location:index.php?p=manage-unit_category");
    exit();
}

function check_cate_exist($conn, $data, $edit = false){
    if (!$edit){
        $stmt = $conn->prepare("SELECT unit_category FROM unit WHERE name =: name");
    } else {
        $stmt = $conn->prepare("SELECT name FROM unit_category WHERE name =:name AND id != :id");
        $stmt->bindParam(":id", $data['id'], PDO::PARAM_STR);
    }

    $stmt->bindParam(":name",$data['name'], PDO::PARAM_STR);
    $stmt->execute();
    $count = $stmt->rowCount();

    if ($count > 0){
        return false;
    } 
    return true;
}

function delete_modules ($conn, $id, $modules){
    $stmt = $conn->prepare("DELETE FROM $modules WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();

}

function create_faqs ($conn, $data, &$errors){
    $stmt_check = $conn->prepare("SELECT * FROM faqs WHERE question =:question");
    $stmt_check->bindParam(":question", $data['question'], PDO::PARAM_STR);
    $stmt_check->execute();

    $count = $stmt_check->rowCount();

    if ($count == 0){
        $stmt = $conn->prepare("INSERT INTO faqs (question, answer, status) VALUES (:question, :answer, :status)");
        $stmt->bindParam(":question", $data['question'], PDO::PARAM_STR);
        $stmt->bindParam(":answer", $data['answer'], PDO::PARAM_STR);
        $stmt->bindParam(":status", $data['status'], PDO::PARAM_INT);
        $stmt->execute();

        header("location:index.php?p=manage-faqs");
        exit();
    } else {
        $errors = 'Data is exist';
    }
}

function edit_faqs ($conn, $data, &$errors){
    $stmt = $conn->prepare("UPDATE faqs SET question=:question,answer=:answer,status=:status WHERE id = :id");
    
    $stmt->bindParam(":question", $data["question"], PDO::PARAM_STR);
    $stmt->bindParam(":answer", $data["answer"], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
    $stmt->bindParam(":id", $data["id"], PDO::PARAM_INT);
    $stmt->execute();

    header("location:index.php?p=manage-faqs");
    exit();
  
}

function create_unit ($conn, $data, &$errors){
    $stmt_check = $conn->prepare("SELECT * FROM unit WHERE name =:name");
    $stmt_check->bindParam(":name", $data['name'], PDO::PARAM_STR);
    $stmt_check->execute();

    $count = $stmt_check->rowCount();

    if ($count == 0){
        $stmt = $conn->prepare("INSERT INTO unit (name, compare, history, unit_category_id) VALUES (:name, :compare, :history, :unit_category_id)");
        $stmt->bindParam(":name", $data['name'], PDO::PARAM_STR);
        $stmt->bindParam(":compare", $data['compare'], PDO::PARAM_STR);
        $stmt->bindParam(":history", $data['history'], PDO::PARAM_STR);
        $stmt->bindParam(":unit_category_id", $data['unit_category_id'], PDO::PARAM_STR);
        $stmt->execute();   

        header("location:index.php?p=manage-unit");
        exit();
    } else {
        $errors = 'Data is exist';
    }
}

function edit_unit($conn, $data){
    $stmt = $conn->prepare("UPDATE unit SET name =:name, compare=:compare, history=:history, unit_category_id =:unit_category_id WHERE id =:id");

    $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt->bindParam(":compare", $data["compare"], PDO::PARAM_STR);
    $stmt->bindParam(":history", $data["history"], PDO::PARAM_STR);
    $stmt->bindParam(":unit_category_id", $data["unit_category_id"], PDO::PARAM_INT);
    $stmt->bindParam(":id", $data["id"], PDO::PARAM_INT);
    $stmt->execute();
    
    header("location:index.php?p=manage-unit");
    exit();
}

function check_modules_exist($conn, $data, $modules, $edit = false){
    if (!$edit){
        $stmt = $conn->prepare("SELECT name FROM $modules WHERE name =: name");
    } else {
        $stmt = $conn->prepare("SELECT name FROM $modules WHERE name =:name AND id != :id");
        $stmt->bindParam(":id", $data['id'], PDO::PARAM_STR);
    }

    $stmt->bindParam(":name",$data['name'], PDO::PARAM_STR);
    $stmt->execute();
    $count = $stmt->rowCount();

    if ($count > 0){
        return false;
    } 
    return true;
}

function create_posts ($conn, $data, &$errors){
    $stmt_check = $conn->prepare("SELECT * FROM posts WHERE title =:title");
    $stmt_check->bindParam(":title", $data['title'], PDO::PARAM_STR);
    $stmt_check->execute();

    $count = $stmt_check->rowCount();

    if ($count == 0){
        $stmt = $conn->prepare("INSERT INTO posts (title,slug, content, images, user_id, source, status) VALUES (:title,:slug, :content, :images,:user_id, :source, :status)");
        $stmt->bindParam(":title", $data['title'], PDO::PARAM_STR);
        $stmt->bindParam(":slug", $data['slug'], PDO::PARAM_STR);
        $stmt->bindParam(":content", $data['content'], PDO::PARAM_STR);
        $stmt->bindParam(":images", $data['images'], PDO::PARAM_STR);
        $stmt->bindParam(":user_id", $data['user_id'], PDO::PARAM_INT);
        $stmt->bindParam(":source", $data['source'], PDO::PARAM_STR);
        $stmt->bindParam(":status", $data['status'], PDO::PARAM_INT);
        $stmt->execute();   

        move_uploaded_file($data["tmp_name"], $data["path_image"]);

        header("location:index.php?p=manage-posts");
        exit();
    } else {
        $errors = 'Data is exist';
    }
}

function get_user($conn, $id){
    $stmt = $conn->prepare("SELECT * FROM user WHERE id=:id");
    $stmt-> bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    return $data;
}

function check_id ($conn, $modules, $id){
    $stmt = $conn->prepare("SELECT * FROM $modules WHERE id =:id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    $count = $stmt->rowCount();
    if ($count > 0){
        return true;
    } else {
        return false;
    }
}

function edit_posts ($conn, $data){
    if (empty($data["images"])){
        $stmt = $conn->prepare("UPDATE `posts` SET `title`=:title,`content`=:content,`slug`=:slug,`status`=:status, source=:source WHERE id=:id");
    } else {
        $check = $conn->prepare("SELECT images FROM posts WHERE id=:id");
        $check->bindParam(":id", $data["id"], PDO::PARAM_INT);
        $check->execute();
        $data_image = $check->fetch(PDO::FETCH_ASSOC);

        if (file_exists('../uploads/posts/'.$data_image["images"])){
            unlink('../uploads/posts/'.$data_image["images"]);
        }

        move_uploaded_file($data["tmp_name"], $data["path_image"]);

        $stmt = $conn->prepare("UPDATE posts SET title=:title, slug=:slug, content=:content, images=:images, source=:source, post_category_id=:post_category_id, status=:status WHERE id=:id");
        $stmt->bindParam(":images", $data["images"], PDO::PARAM_STR);
    }

    $stmt->bindParam(":title", $data["title"], PDO::PARAM_STR);
    $stmt->bindParam(":slug", $data["slug"], PDO::PARAM_STR);
    $stmt->bindParam(":content", $data["content"], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data["status"], PDO::PARAM_INT);
    $stmt->bindParam(":source", $data["source"], PDO::PARAM_STR);
    $stmt->bindParam(":id", $data["id"], PDO::PARAM_INT);
    $stmt->execute();
    
    header("location:index.php?p=manage-posts");
    exit();
}

function delete_posts ($conn, $id) {
    $check = $conn->prepare("SELECT images FROM posts WHERE id = :id");
    $check->bindParam(":id", $id, PDO::PARAM_INT);
    $check->execute();
    $data = $check->fetch(PDO::PARAM_STR);

    if (file_exists('../uploads/posts/'. $data["images"])) {
        unlink('../uploads/posts/'. $data["images"]);
    }

    $stmt = $conn->prepare("DELETE FROM posts WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
}

function create_user ($conn, $data){
    $stmt = $conn->prepare("INSERT INTO user (fullname, email, password, phone, level, images, status) VALUES (:fullname, :email, :password, :phone, :level, :images, :status)");
    $stmt->bindParam(":fullname", $data['fullname'], PDO::PARAM_STR);
    $stmt->bindParam(":email", $data['email'], PDO::PARAM_STR);
    $stmt->bindParam(":password", $data['password'], PDO::PARAM_STR);
    $stmt->bindParam(":phone", $data['phone'], PDO::PARAM_STR);
    $stmt->bindParam(":level", $data['level'], PDO::PARAM_INT);
    $stmt->bindParam(":images", $data['images'], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data['status'], PDO::PARAM_INT);
    $stmt->execute();   

    move_uploaded_file($data["tmp_name"], $data["path_image"]);

    header("location:index.php?p=manage-user");
    exit();
}

function edit_user ($conn, $data){
    if (empty($data["images"])){
        $stmt = $conn->prepare("UPDATE `user` SET fullname=:fullname, password=:password, phone=:phone,  status=:status  WHERE id=:id");
    } else {
        $check = $conn->prepare("SELECT images FROM user WHERE id=:id");
        $check->bindParam(":id", $data["id"], PDO::PARAM_INT);
        $check->execute();
        $data_image = $check->fetch(PDO::FETCH_ASSOC);

        if (file_exists('../uploads/user/'.$data_image["images"])){
            unlink('../uploads/user/'.$data_image["images"]);
        }
        move_uploaded_file($data["tmp_name"], $data["path_image"]);
        $stmt = $conn->prepare("UPDATE user SET fullname=:fullname, password=:password, phone=:phone,  status=:status, images=:images WHERE id=:id");
        $stmt->bindParam(":images", $data["images"], PDO::PARAM_STR);
    }
    $stmt->bindParam(":fullname", $data['fullname'], PDO::PARAM_STR);
    $stmt->bindParam(":password", $data['password'], PDO::PARAM_STR);
    $stmt->bindParam(":phone", $data['phone'], PDO::PARAM_STR);
    $stmt->bindParam(":status", $data['status'], PDO::PARAM_INT);
    $stmt->bindParam(":id", $data['id'], PDO::PARAM_INT);
    $stmt->execute();
    
    header("location:index.php?p=manage-user");
    exit();
}

function delete_user ($conn, $id) {
    $check = $conn->prepare("SELECT images FROM user WHERE id = :id");
    $check->bindParam(":id", $id, PDO::PARAM_INT);
    $check->execute();
    $data = $check->fetch(PDO::PARAM_STR);

    if (file_exists('../uploads/user/'. $data["images"])) {
        unlink('../uploads/user/'. $data["images"]);
    }

    $stmt = $conn->prepare("DELETE FROM user WHERE id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
}

function show_writer ($conn, $modules = 'user'){
    $stmt = $conn->prepare("SELECT * FROM $modules");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function list_modules_user ($conn, $modules = 'unit_category'){
    $stmt = $conn->prepare("SELECT * FROM $modules");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function display_table_data ($conn, $id){
    $stmt = $conn->prepare("SELECT * FROM unit WHERE unit.unit_category_id = $id");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function list_post ($conn){
    $stmt = $conn->prepare("SELECT * FROM posts ");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
    
}

function show_article ($conn, $id) {
    $stmt = $conn->prepare("SELECT * FROM posts WHERE id = $id");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function latest_post ($conn, $id) {
    $stmt = $conn->prepare("SELECT * FROM posts WHERE id != $id LIMIT 3");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function lastest_history ($conn, $id) {
    $stmt = $conn->prepare("SELECT * FROM posts WHERE id != $id AND post_category_id = 2 LIMIT 6");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function readmore ($string){
    $string = substr($string, 0, 300);
    echo $string. "...";
}

function up_contact ($conn, $data){
    $stmt = $conn->prepare("INSERT INTO contact (name, email, content) VALUES (:name, :email, :content)");
    $stmt->bindParam(":name", $data["name"], PDO::PARAM_STR);
    $stmt->bindParam(":email", $data["email"], PDO::PARAM_STR);
    $stmt->bindParam(":content", $data["content"], PDO::PARAM_STR);
    $stmt->execute();
}

function show_data_cate($conn){
    $stmt = $conn->prepare("SELECT * FROM unit_category");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function show_data_unit($conn, $id){
    $stmt = $conn->prepare("SELECT * FROM unit WHERE unit_category_id = :id LIMIT 2");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function show_all_unit( $conn){
    $stmt = $conn->prepare("SELECT * FROM unit ");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
function data_recursive ($conn){
    $stmt = $conn->prepare("SELECT * FROM unit_category");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


?>