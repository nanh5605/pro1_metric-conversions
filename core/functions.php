<?php 
function active_menu ($module, $type = 'active') {
    if (isset($_GET["p"]) && in_array($_GET["p"], $module)) {
        if ($type == 'active') {
            echo 'active';
        } else {
            echo 'menu-open';
        }
    }
}

function keep_value_input ($name, $value = null){
    if (isset($_POST[$name])){
        echo 'value="'.$_POST[$name].'"';
    } 
    if (!is_null($value)){
        echo 'value="'.$value.'"';
    }
}

function keep_value_area ($name, $value = null){
    if (isset($_POST[$name])){
        echo $_POST[$name];
    } 
    if (!is_null($value)){
        echo $value;
    }
}

function recursive ($data, $selected ,$parent = 0, $str = "---| ") {
    foreach ($data as $item) {
        if ($item["parent"] == $parent) {
            $selected_option = ($item["id"] == $selected) ? 'selected' : '';
            echo '<option value="'.$item["id"].'" '.$selected_option.'>'.$str.$item["name"].'</option>';
            recursive ($data, $selected, $item["id"], $str . "---| ");
        }
    }
}

function recursive_user ($data, $selected ) {
    foreach ($data as $item) {
        $selected_option = ($item["id"] == $selected) ? 'selected' : '';
        echo '<option value="'.$item["id"].'" '.$selected_option.'>'.$item["fullname"].'</option>';
    }
}

function check_extension ($string){
    $position = strrpos($string, '.');
    $tailname = substr($string, $position + 1);
    if ($tailname != 'png' && $tailname != 'jpeg' && $tailname != 'gif' && $tailname != 'jpg' && $tailname != 'PNG' && $tailname !== 'JPEG' && $tailname !== 'GIF' && $tailname !== 'JPG' ){
        return false;
    } else {
        return true;
    }
}

function change_name_file ($string) {
    $string = trim($string);
    $string = mb_strtolower($string);
    $string = preg_replace('!\s+!', ' ', $string);
    $string = str_replace(" ",'-', $string);
    return $string;
}

function resetXss ($str) {
    return strip_tags(htmlspecialchars(trim($str)));
}


?>