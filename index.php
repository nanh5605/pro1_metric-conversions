<!DOCTYPE html>
<html lang="en">
<?php 
ob_start();
include 'core/config.php';
include 'core/connect.php';
include 'core/model.php';
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    
    <?php include 'blocks/head.php' ?>
    
</head>
<body id="body">
        <header class="navigation fixed-top" style="background-color: #F1F8F4 !important">
            <?php include 'blocks/main_nav.php' ?>
        </header>

        <?php
            if (isset($_GET['p'])){
                $p = $_GET['p'];

                switch ($p) {
                    case 'all-converter' :
                        include 'pages/allconverter/index.php';
                        break;
                    case 'about-us':
                        include 'pages/aboutus/index.php';
                        break;
                    case 'article':
                        include 'pages/posts/index.php';
                        break;
                    case 'help':
                        include 'pages/help/index.php';
                        break;
                    default:
                        include 'pages/home/index.php';
                }
            } else {
                include 'pages/home/index.php';
            }
        ?>
        <?php include 'blocks/footer.php' ?>
        <?php include 'blocks/script.php' ?>

        <script type="text/javascript" >
            $(document).ready(function (){
                
                $("input[name='fromVal']").keyup(delay(function (){
                    var valInput = $(this).val();
                    function displayVals (){
                        var val1 = $("select[name='calFrom']").val();
                        var val2 = $("select[name='calTo']").val();
                        var pheptinh = valInput * (val1 / val2) ;
                        if (isNaN(pheptinh) == true){
                            $("input[name='toVal']").val("Please provide a valid number!");
                        } else {
                            $("input[name='toVal']").val(pheptinh);
                        }
                    }
                    $( "select[name='calFrom'], select[name='calTo']" ).change( displayVals );
                    displayVals ();
                }, 100));
            });
            function delay(callback, ms) {
                var timer = 0;
                return function() {
                    var context = this, args = arguments;
                    clearTimeout(timer);
                    timer = setTimeout(function () {
                        callback.apply(context, args);
                    }, ms || 0);
                };
            }
        </script>
    </body>
</html>


