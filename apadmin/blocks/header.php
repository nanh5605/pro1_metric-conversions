<?php 
    $data = array (
        'create-unit' => 'Unit',
        'edit-unit' => 'Unit',
        'manage-unit' =>'Unit',
        'create-faqs' => 'FAQs',
        'edit-faqs' => 'FAQs',
        'manage-faqs' => 'FAQs',
        'reply-contact' => 'Contact',
        'manage-contact' => 'Contact',
        'create-user' => 'User',
        'edit-user' => 'User',
        'manage-user' => 'User',
        'create-posts' => 'Posts',
        'edit-posts' => 'Posts',
        'manage-posts' => 'Posts',
    );
    if (isset($_GET["p"])){
        $p = $_GET["p"];
        foreach ($data as $key => $value){
            if ($key == $p){
?>

<div class="content-header">
    <div class="container-fluid">
    <h1 class="m-0"><?php echo $value ?></h1>
    </div><!-- /.container-fluid -->
</div>
<?php }}}?>