<aside class="main-sidebar sidebar-dark-primary elevation-4">
<!-- Brand Logo -->
<a href="#" class="brand-link">
    <span class="pl-4 brand-text font-weight-light">Metric Conversions</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <div class="image">
        <img src="../uploads/user/<?php echo $_SESSION["login"]["images"] ?>" class="img-circle elevation-2" alt="User Image">
    </div>
    <div class="info">
        <a href="#" class="d-block">
            <?php echo $_SESSION["login"]["fullname"] ?>
        </a>
    </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
    <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
        <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
        </button>
        </div>
    </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <!--Category-->
        <li class="nav-item <?php active_menu (['manage-unit_category', 'edit-unit_category', 'create-unit_category'], 'menu-open') ?>">
            <a href="#" class="nav-link <?php active_menu (['manage-category', 'edit-category', 'create-category']) ?>">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                Unit Category
                <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="index.php?p=create-unit_category" class="nav-link <?php active_menu (['create-category']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Create category</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="index.php?p=manage-unit_category" class="nav-link <?php active_menu (['manage-category', 'edit-category']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Manage category</p>
                </a>
                </li>
            </ul>
        </li>
        <!--unit-->
        <li class="nav-item <?php active_menu (['manage-unit', 'edit-unit', 'create-unit'], 'menu-open') ?>">
            <a href="#" class="nav-link <?php active_menu (['manage-unit', 'edit-unit', 'create-unit']) ?>">
                <i class="nav-icon fas fa-table"></i>
                <p>
                Unit
                <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="index.php?p=create-unit" class="nav-link <?php active_menu (['create-unit']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Create unit</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="index.php?p=manage-unit" class="nav-link <?php active_menu (['manage-unit', 'edit-unit']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Manage unit</p>
                </a>
                </li>
            </ul>
        </li>
        <!--User-->
        <li class="nav-item <?php active_menu (['manage-user', 'edit-user', 'create-user'], 'menu-open') ?>">
            <a href="#" class="nav-link <?php active_menu (['manage-user', 'edit-user', 'create-user']) ?>">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                User
                <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="index.php?p=create-user" class="nav-link <?php active_menu (['create-user']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Create user</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="index.php?p=manage-user" class="nav-link <?php active_menu (['manage-user', 'edit-user']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Manage user</p>
                </a>
                </li>
            </ul>
        </li>
        <!--posts-->
        <li class="nav-item <?php active_menu (['manage-posts', 'edit-posts', 'create-posts'], 'menu-open') ?>">
            <a href="#" class="nav-link <?php active_menu (['manage-posts', 'edit-posts', 'create-posts']) ?>">
                <i class="nav-icon fas fa-table"></i>
                <p>
                Posts
                <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="index.php?p=create-posts" class="nav-link <?php active_menu (['create-posts']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Create post</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="index.php?p=manage-posts" class="nav-link <?php active_menu (['manage-posts', 'edit-posts']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Manage post</p>
                </a>
                </li>
            </ul>
        </li>
        <!--faqs -->
        <li class="nav-item <?php active_menu (['manage-faqs', 'edit-faqs', 'create-faqs'], 'menu-open') ?>">
            <a href="#" class="nav-link <?php active_menu (['manage-faqs', 'edit-faqs', 'create-faqs']) ?>">
                <i class="nav-icon fas fa-question-circle"></i>
                <p>
                FAQs
                <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                <a href="index.php?p=create-faqs" class="nav-link <?php active_menu (['create-faqs']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Create FAQs</p>
                </a>
                </li>
                <li class="nav-item">
                <a href="index.php?p=manage-faqs" class="nav-link <?php active_menu (['manage-faqs', 'edit-faqs']) ?>">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Manage FAQs</p>
                </a>
                </li>
            </ul>
        </li>
        
    </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>