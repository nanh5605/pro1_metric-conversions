<!-- jQuery -->
<script src="public/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="public/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Bootstrap Switch -->
<script src="public/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="public/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="public/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="public/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="public/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="public/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="public/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="public/assets/plugins/jszip/jszip.min.js"></script>
<script src="public/assets/plugins/pdfmake/pdfmake.min.js"></script>
<script src="public/assets/plugins/pdfmake/vfs_fonts.js"></script>
<script src="public/assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="public/assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="public/assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- AdminLTE App -->
<script src="public/assets/dist/js/adminlte.min.js"></script>
<!-- myscript -->
<script src="public/assets/dist/js/myscript.js"></script>
