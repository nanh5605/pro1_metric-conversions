<?php 
session_start();
ob_start();

if (!isset($_SESSION["login"]["email"])){
    header("location:login.php");
    exit();
}

if (!isset($_SESSION["login"])){
    header("location:login.php");
    exit();
}



include '../core/config.php';
include '../core/connect.php';
include '../core/functions.php';
include '../core/model.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include 'blocks/head.php' ?>
    </head>
    <body class="hold-transition sidebar-mini dark-mode">
        <div class="wrapper">
            <!-- Navbar -->
            <?php include 'blocks/navbar.php' ?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php include 'blocks/sidebar.php' ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content">
                <?php include 'blocks/header.php' ?>
                </div>
                
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <?php 
                        if (isset($_GET["p"])) {
                            $p = $_GET["p"];
                            switch ($p) {
                                case 'create-unit_category':
                                    include 'modules/unit_category/create.php';
                                    break;
                                case 'edit-unit_category':
                                    include 'modules/unit_category/edit.php';
                                    break;
                                case 'delete-unit_category':
                                    include 'modules/unit_category/delete.php';
                                    break;
                                case 'manage-unit_category':
                                    include 'modules/unit_category/index.php';
                                    break;
                                case 'create-unit':
                                    include 'modules/unit/create.php';
                                    break;
                                case 'edit-unit':
                                    include 'modules/unit/edit.php';
                                    break;
                                case 'delete-unit':
                                    include 'modules/unit/delete.php';
                                    break;
                                case 'manage-unit':
                                    include 'modules/unit/index.php';
                                    break;
                                case 'create-faqs':
                                    include 'modules/faqs/create.php';
                                    break;
                                case 'edit-faqs':
                                    include 'modules/faqs/edit.php';
                                    break;
                                case 'delete-faqs':
                                    include 'modules/faqs/delete.php';
                                    break;
                                case 'manage-faqs':
                                    include 'modules/faqs/index.php';
                                    break;
                                case 'create-posts':
                                    include 'modules/posts/create.php';
                                    break;
                                case 'edit-posts':
                                    include 'modules/posts/edit.php';
                                    break;
                                case 'delete-posts':
                                    include 'modules/posts/delete.php';
                                    break;
                                case 'manage-posts':
                                    include 'modules/posts/index.php';
                                    break;
                                    
                                case 'create-user':
                                    include 'modules/user/create.php';
                                    break;
                                case 'edit-user':
                                    include 'modules/user/edit.php';
                                    break;
                                case 'delete-user':
                                    include 'modules/user/delete.php';
                                    break;
                                case 'manage-user':
                                    include 'modules/user/index.php';
                                    break;
                                default:
                                    include 'modules/unit_category/index.php';
                            }
                        } else {
                            include 'modules/unit_category/index.php';
                        }
                        ?>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Control Sidebar -->
            <?php include 'blocks/rightside.php' ?>
            <!-- /.control-sidebar -->

            <!-- Main Footer -->
            <?php include 'blocks/footer.php' ?>
        </div>
        <!-- ./wrapper -->

        <!-- REQUIRED SCRIPTS -->

        <?php include 'blocks/foot.php' ?>
    </body>
</html>
