<?php 
session_start();
ob_start();

if (isset($_SESSION['login'])){
    header ("location:index.php?p=manage-unit_category");
    exit();
}

include '../core/config.php';
include '../core/connect.php';
include '../core/auth.php';
?>
<?php
$errors = array();

if (isset($_POST['login'])){
    if (empty($_POST['email'])){
        $errors[] = 'jdk';
    }
    if (empty($_POST['password'])){
        $errors[] = 'jdk';
    }
    if (empty($errors)){
        $data = array (
            'email' => $_POST['email'],
            'password' => md5($_POST['password']),
            'level' => 1
        );

        $result = login ($conn, $data);

        if ($result){
            $user = get_user ($conn, $data);
            $_SESSION["login"]["id"] = $user["id"];
            $_SESSION["login"]["email"] = $user["email"];
            $_SESSION["login"]["fullname"] = $user["fullname"];
            $_SESSION["login"]["images"] = $user["images"];
            $_SESSION["login"]["level"] = $user["level"];
            header("location:index.php?p=manage-unit_category");
            exit();
        } else {
            $errors[] = "Account not exist";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="public/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="public/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="public/assets/dist/css/adminlte.min.css">

</head>
<body class="hold-transition login-page" >
<div class="login-box">
  <div class="login-logo">
    <a href="public/assets/index2.html"><b>METRIC CONVERSIONS</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
      
    <div class="card-body login-card-body">
      <p class="login-box-msg">Login</p>
      <?php if (!empty($errors)) {?>
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-ban"></i> Alert!</h5>
            <?php foreach ($errors as $error) {
                echo "<li>".$error."</li>";
            }?>

        </div>
        <?php } ?>
      <form action="" method="post" id="quickForm">
        <div class="input-group mb-3">
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <button type="submit" name="login" class="btn btn-primary btn-block">Sign In</button>
        
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="public/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="public/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="public/assets/dist/js/adminlte.min.js"></script>


</body>
</html>
