<?php
$data = list_modules ($conn, "unit"); 
$data2 = list_modules ($conn, "unit_category");
 ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">List of Unit</h3>
    </div>
    <div class="card-body">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $item) {
                        foreach ($data2 as $item2 ){
                            if ($item['unit_category_id'] == $item2['id']){
                    ?>
                <tr>
                    <td><?php echo $item['id'] ?></td>
                    <td><?php echo $item['name'] ?></td>
                    <td><?php echo $item2['name'] ?></td>
                    <td>
                        <a class="confirmDelete" href="index.php?p=delete-unit&id=<?php echo $item['id'] ?>">Delete</a> | 
                        <a href="index.php?p=edit-unit&id=<?php echo $item['id'] ?>">Edit</a>
                    </td>
                </tr>
                <?php }}}?>
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>