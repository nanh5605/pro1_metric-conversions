<?php
$errors = array();
$recursive = data_recursive ($conn);

if (isset($_POST['create'])){
    if (empty($_POST['name'])){
        $errors[] = 'Please enter name of unit';
    } 
    if (!is_numeric($_POST['compare'])){
        $errors[] = 'Invalid number';
    }
   
    if (empty($errors)){
        $data = array (
            'name' => resetXss ($_POST['name']),
            'unit_category_id' => $_POST['unit_category_id'],
            'compare' => resetXss ($_POST['compare']),
            'history' => resetXss ($_POST['history'])
        );

        create_unit ($conn, $data, $errors[]);
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create Unit</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" encytype="multipart/form-data">
        <div class="card-body">
        <div class="form-group">
                <label>Category</label>
                <select name="unit_category_id" class="form-control">
                    <?php 
                        if (isset($_POST["unit_category_id"])) {
                            recursive ($recursive, $_POST["unit_category_id"]);
                        } else {
                            recursive ($recursive, 0);
                        }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control"  name="name"  placeholder="Please enter unit's name" <?php keep_value_input('name') ?> >
            </div>
            <div class="form-group">
                <label>Compared</label>
                <input type="text" class="form-control"  name="compare"  placeholder="Please enter comparation" <?php keep_value_input('compare') ?> >
            </div>
            <div class="form-group">
                <label>History</label>
                <textarea name="history" id="" class="form-control" placeholder="Text here"><?php  keep_value_area ('history') ?></textarea>
                <script>
                    CKEDITOR.replace( 'history' );
                </script>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="create" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
