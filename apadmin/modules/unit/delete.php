<?php 
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    settype($id, 'int');

    if ($id != 0) {
        delete_modules ($conn, $id, 'unit');
        
        header("location:index.php?p=manage-unit");
        exit();
    } else {
        header("location:index.php?p=manage-unit");
        exit();
    }
} else {
    header("location:index.php?p=manage-unit");
    exit();
}
?>