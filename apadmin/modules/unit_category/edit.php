<?php
if (!isset($_GET["id"])){
    header("location:index.php?p=manage-unit_category");
    exit();
} 
$id = $_GET['id'];
if ($id == 0){
    header("location:index.php?p=manage-unit_category");
    exit();
}
else {
    settype($id, "int");
}

if (!check_id ($conn,'unit_category', $id)){
    header("location:index.php?p=manage-unit_category");
    exit();
}

$old = get_one_modules ($conn, $id, 'unit_category');

$errors = array();
if (isset($_POST['edit'])){
    if (empty($_POST['name'])){
        $errors[] = 'Please enter category name';
    } 
    if (empty($_POST['slug'])){
        $errors[] = 'Please enter slug';
    }
    $name = ($_POST['name']);
    if (empty($errors)){
        $data = array (
            'name' => resetXss ($name),
            'slug' => resetXss ($_POST['slug']),
            'status' => (isset($_POST['status']) && $_POST['status'] == 'on') ? 1: 0,
            'id' => $id
        );
        if ( check_modules_exist($conn, $data, 'unit_category', true)){
            edit_unit_category ($conn, $data);
        } else {
            $errors[] = "Data is exist";
        }
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Unit Category</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" encytype="multipart/form-data">
        <div class="card-body">
            <div class="form-group">
                <label>Category name</label>
                <input type="text" class="form-control convert_slug" data-slug="slug" name="name"  placeholder="Please enter category name" <?php keep_value_input('name', $old['name']) ?> >
            </div>
            <div class="form-group">
                <label>Slug</label>
                <input type="text" class="form-control" name="slug" placeholder="Please enter slug" <?php keep_value_input('slug', $old['slug']) ?>>
            </div>
            <input type="checkbox" name="status" class="bootstrap_switch" checked data-bootstrap-switch data-off-color="danger" data-on-color="success"
            <?php 
                if ($old['status'] == 1){
                    echo 'checked';
                }
            ?>>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="edit" class="btn btn-primary">Edit</button>
        </div>
    </form>
</div>