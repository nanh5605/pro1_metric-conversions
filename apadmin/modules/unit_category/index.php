<?php
$data = list_modules ($conn, "unit_category"); 
 ?>
 <div class="content-header">
    <div class="container-fluid">
    <h1 class="m-0">Unit Category</h1>
    </div><!-- /.container-fluid -->
</div>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">List of Unit Category</h3>
    </div>
    <div class="card-body">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Category name</th>
                    <th>Slug</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $item) {?>
                <tr>
                    <td><?php echo $item['id'] ?></td>
                    <td><?php echo $item['name'] ?></td>
                    <td><?php echo $item['slug'] ?></td>
                    <td>
                        <a class="confirmDelete" href="index.php?p=delete-unit_category&id=<?php echo $item['id'] ?>">Delete</a> | 
                        <a href="index.php?p=edit-unit_category&id=<?php echo $item['id'] ?>">Edit</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
            <tfoot>
                <tr>
                <th>ID</th>
                    <th>Category name</th>
                    <th>Slug</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>