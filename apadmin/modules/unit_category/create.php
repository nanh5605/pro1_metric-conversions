<?php
$errors = array();
if (isset($_POST['create'])){
    if (empty($_POST['name'])){
        $errors[] = 'Please enter category name';
    } 
    if (empty($_POST['slug'])){
        $errors[] = 'Please enter slug';
    }
    $name = ($_POST['name']);
    if (empty($errors)){
        $data = array (
            'name' => resetXss ($name),
            'slug' => resetXss ($_POST['slug']),
            'status' => (isset($_POST['status']) && $_POST['status'] == 'on') ? 1: 0
        );

        create_unit_category ($conn, $data, $errors[]);
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="content-header">
    <div class="container-fluid">
    <h1 class="m-0">Unit Category</h1>
    </div><!-- /.container-fluid -->
</div>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create Unit Category</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" encytype="multipart/form-data">
        <div class="card-body">
            <div class="form-group">
                <label>Category name</label>
                <input type="text" class="form-control convert_slug" name="name" data-slug="slug" placeholder="Please enter unit's name" <?php keep_value_input('name') ?> >

               
            </div>
            <div class="form-group">
                <label>Slug</label>
                <input type="text" class="form-control" name="slug" placeholder="Please enter slug" <?php keep_value_input('slug') ?>>
            </div>
            <input type="checkbox" name="status" class="bootstrap_switch" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="create" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>