<?php
if (!isset($_GET["id"])){
    header("location:index.php?p=manage-faqs");
    exit();
} 
$id = $_GET['id'];
if ($id == 0){
    header("location:index.php?p=manage-faqs");
    exit();
}
else {
    settype($id, "int");
}

if (!check_id ($conn,'faqs', $id)){
    header("location:index.php?p=manage-faqs");
    exit();
}

$old = get_one_modules ($conn, $id, 'faqs');

$errors = array();
if (isset($_POST['edit'])){
    if (empty($errors)){
        $data = array (
            'question' => $_POST['question'],
            'answer' => $_POST['answer'],
            'status' => (isset($_POST['status']) && $_POST['status'] == 'on') ? 1: 0,
            'id' => $id
        );

        edit_faqs ($conn, $data, $errors[]);
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit FAQs</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" encytype="multipart/form-data">
        <div class="card-body">
            <div class="form-group">
                <label>Question</label>
                <textarea name="question" class="form-control"><?php  keep_value_area ('question', $old['question']) ?></textarea>
                <script>
                    CKEDITOR.replace( 'question' );
                </script>
            </div>
            <div class="form-group">    
                <label>Answer</label>
                <textarea name="answer" class="form-control"><?php  keep_value_area ('answer',$old['answer']) ?></textarea>
                <script>
                    CKEDITOR.replace( 'answer' );
                </script>
            </div>
            <input type="checkbox" name="status" class="bootstrap_switch" checked data-bootstrap-switch data-off-color="danger" data-on-color="success"
            <?php 
                if ($old['status'] == '1'){
                    echo 'checked';
                } else {
                    echo '';
                }
            ?>>        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="edit" class="btn btn-primary">edit</button>
        </div>
    </form>
</div>