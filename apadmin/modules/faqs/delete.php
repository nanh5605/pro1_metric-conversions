<?php 
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    settype($id, 'int');

    if ($id != 0) {
        delete_modules ($conn, $id, 'faqs');

        header("location:index.php?p=manage-faqs");
        exit();
    } else {
        header("location:index.php?p=manage-faqs");
        exit();
    }
} else {
    header("location:index.php?p=manage-faqs");
    exit();
}
?>