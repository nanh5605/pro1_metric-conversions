<?php
$data = list_modules ($conn, "faqs"); 
 ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">List of Questions</h3>
    </div>
    <div class="card-body">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Question</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $item) {?>
                <tr>
                    <td><?php echo $item['id'] ?></td>
                    <td><?php echo $item['question'] ?></td>
                    <td><?php echo ($item['status'] == 1) ? "On" : "Off" ?></td>
                    <td>
                        <a class="confirmDelete" href="index.php?p=delete-faqs&id=<?php echo $item['id'] ?>">Delete</a> | 
                        <a href="index.php?p=edit-faqs&id=<?php echo $item['id'] ?>">Edit</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Question</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>