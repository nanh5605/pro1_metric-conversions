<?php
$errors = array();
if (isset($_POST['create'])){
    if (empty($_POST['question'])){
        $errors[] = 'Please enter question';
    } 
    if (empty($_POST['answer'])){
        $errors[] = 'Please enter answer';
    }
    if (empty($errors)){
        $data = array (
            'question' => resetXss ($_POST['question']),
            'answer' => resetXss ($_POST['answer']),
            'status' => (isset($_POST['status']) && $_POST['status'] == 'on') ? 1: 0
        );
        create_faqs ($conn, $data, $errors[]);
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create FAQs</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" encytype="multipart/form-data">
        <div class="card-body">
            <div class="form-group">
                <label>Question</label>
                <textarea name="question" class="form-control"><?php  keep_value_area ('question') ?></textarea>
                <script>
                    CKEDITOR.replace( 'question' );
                </script>
            </div>
            <div class="form-group">    
                <label>Answer</label>
                <textarea name="answer" class="form-control"><?php  keep_value_area ('answer') ?></textarea>
                <script>
                    CKEDITOR.replace( 'answer' );
                </script>
            </div>
            <input type="checkbox" name="status" class="bootstrap_switch" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="create" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>