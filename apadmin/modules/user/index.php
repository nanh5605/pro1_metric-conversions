<?php
$data = list_modules ($conn, "user"); 
 ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">List of Member</h3>
    </div>
    <div class="card-body">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Image</th>
                    <th>Full name</th>
                    <th>Email</th>
                    <th>Level</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $item) {?>
                <tr>
                    <td><?php echo $item['id'] ?></td>
                    <td><img src="../uploads/user/<?php echo $item['images'] ?>" style="height:100px; width: 100px" alt=""></td>
                    <td><?php echo $item['fullname'] ?></td>
                    <td><?php echo $item['email'] ?></td>
                    <td><?php echo ($item['id'] == 1) ? '<strong>Super Admin</strong>' : 'Admin' ?></td>
                    <td>
                        <a class="confirmDelete" href="index.php?p=delete-user&id=<?php echo $item['id'] ?>">Delete</a> | 
                        <a href="index.php?p=edit-user&id=<?php echo $item['id'] ?>">Edit</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
            <tfoot>
                <tr>
                <th>ID</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Level</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>