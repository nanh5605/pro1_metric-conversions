<?php
if (!isset($_GET["id"])){
    header("location:index.php?p=manage-user");
    exit();
}
$id = $_GET["id"];
if ($id == 0){
    header("location:index.php?p=manage-user");
    exit();
} else {
    settype($id, 'int');
}

if (!check_id ($conn,'user', $id)){
    header("location:index.php?p=manage-user");
    exit();
}
$errors = array();
$old = get_one_modules ($conn, $id, 'user');
$user = get_user($conn, $id);

$edit_myself = null;
if ($_SESSION['login']['id'] == $id){
    $edit_myself = true;
} else {
    $edit_myself = false;
}

if ($_SESSION['login']['id'] != 1 && ($_GET['id'] == 1 || $edit_myself == false)){
    echo '<script>
        alert ("You cannot edit this member")
        window.location.href="index.php?p=manage-user"
    </script>';
    exit();
}

if (isset($_POST['edit'])){

    if (empty($_POST["fullname"])) {
        $errors[] = "Please enter full name";
    }
    if (empty($_POST["phone"])) {
        $errors[] = "Please enter phone number";
    }
    if (strlen($_POST["phone"]) != 10){
        $errors[] = "Phone number must be have 10 number";
    }
    
    $data = array (
        'fullname' => resetXss ($_POST['fullname']),
        'phone' => resetXss ($_POST['phone']),
        'level' => $_POST["level"],
        'status' => (isset($_POST['status']) && $_POST['status'] == 'on') ? 1: 0,
        'tmp_name' => $_FILES["images"]["tmp_name"],
        'path_image' => '../uploads/user/'.time()."_".$_FILES["images"]["name"],
        'id' => $id
    );

    if (empty($_POST["password"])){
        $data["password"] = $user["password"];
    } else {
        if (strlen($_POST["password"]) < 6 ){
            $errors[] = "Password must be at least 6 characters";
        } elseif ($_POST["password"] != $_POST["confirm_password"]){
            $errors[] = "Confirm password is incorrect";
        } else {
            $data["password"] = md5($_POST["password"]);
        }
    }

        if (!empty($_FILES["images"]["name"])) {
            $data['images'] = time()."_".$_FILES["images"]["name"];
        }
        if (empty($errors)){
        edit_user ($conn, $data);
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create user</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            <div class="form-group">
                <label>Full name</label>
                <input type="text" class="form-control" name="fullname"  placeholder="Please enter full name" <?php keep_value_input('fullname', $old["fullname"]) ?> >
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email"  placeholder="Please enter email" <?php keep_value_input('email', $old["email"]) ?> disabled>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password"  placeholder="Please enter new password">
            </div>
            <div class="form-group">
                <label>Confirm password</label>
                <input type="password" class="form-control" name="confirm_password"  placeholder="Please enter confirm password">
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" name="phone" class="form-control" placeholder="Please enter phone number" <?php keep_value_input('phone', $old["phone"]) ?>>
            </div>
            <?php if (!$edit_myself) { ?>
            <div class="form-group">
                <label>Level</label>
                <select name="level" class="form-control">
                    <option value="1" <?php echo $old["level"] == 1 ? 'selected' : '' ?>>Admin</option>
                </select>
            </div>
            <?php } ?>
            <div class="form-group">
                <label>Old image</label>
                <img src="../uploads/user/<?php echo $old['images'] ?>" width="100px" >
            </div>
            <div class="form-group">
                <label>New image</label>
                <input type="file" class="form-control" name="images" >
            </div>
            <input type="checkbox" name="status" class="bootstrap_switch" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="edit" class="btn btn-primary">Edit</button>
        </div>
    </form>
</div>
<?php

