<?php
if ($_SESSION['login']['id'] != 1){
    echo '<script>
        alert ("You are not authorized to add members")
        window.location.href="index.php?p=manage-user"
    </script>';
    exit();
}

$errors = array();

if (isset($_POST['create'])){
    if (empty($_POST["email"])) {
        $errors[] = "Please enter email";
    }
    if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i",$_POST["email"])) {
        $errors[] = "Invalid email";
      }

    if (empty($_POST["password"])) {
        $errors[] = "Please enter password";
    }

    if ($_POST["password"] != $_POST["confirm_password"]) {
        $errors[] = "Confirm password is incorrect";
    }

    if (strlen($_POST["password"]) < 6 ){
        $errors[] = "Password must be at least 6 characters";
    }

    if (empty($_POST["fullname"])) {
        $errors[] = "Please enter full name";
    }

    if (empty($_POST["phone"])) {
        $errors[] = "Please enter phone number";
    }

    if (strlen($_POST["phone"]) != 10){
        $errors[] = "Phone number must be have 10 number";
    }

    if (empty($_FILES["images"]["name"])) {
        $errors[] = "Please choose image";
    } 
    if (!check_extension ($_FILES["images"]["name"])){
        $errors[] = "Please choose image file";
    }
    if (empty($errors)){
        $data = array (
            'fullname' => resetXss ($_POST['fullname']),
            'email' => resetXss ($_POST['email']),
            'phone' => resetXss ($_POST['phone']),
            'password' => md5($_POST["password"]),
            'level' => $_POST["level"],
            'images' => time().'_'.change_name_file ($_FILES["images"]["name"]),
            'status' => (isset($_POST['status']) && $_POST['status'] == 'on') ? 1: 0,
            'tmp_name' => $_FILES["images"]["tmp_name"],
            'path_image' => '../uploads/user/'.time()."_".$_FILES["images"]["name"]
        );
        
        create_user ($conn, $data);
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create user</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            <div class="form-group">
                <label>Full name</label>
                <input type="text" class="form-control" name="fullname"  placeholder="Please enter full name" <?php keep_value_input('fullname') ?> >
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" name="email"  placeholder="Please enter email" <?php keep_value_input('email') ?> >
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password"  placeholder="Please enter password">
            </div>
            <div class="form-group">
                <label>Confirm Password</label>
                <input type="password" class="form-control" name="confirm_password"  placeholder="Please enter confirm password">
            </div>
            <div class="form-group">
                <label>Phone</label>
                <input type="text" name="phone" class="form-control" placeholder="Please enter phone number" <?php keep_value_input('phone') ?>>
            </div>
            <div class="form-group">
                <label>Level</label>
                <select name="level" class="form-control">
                    <option value="1" checked>Admin</option>
                </select>
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control" name="images" >
            </div>
            <input type="checkbox" name="status" class="bootstrap_switch" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="create" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>
<?php

