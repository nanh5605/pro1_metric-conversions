<?php 
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    settype($id, 'int');

    if ($id != 0) {

        if (($id == 1) || $_SESSION['login']['id'] != 1){
            echo '<script>
                alert ("You cannot delete this member")
                window.location.href="index.php?p=manage-user"
            </script>';
            exit();
        }

        delete_user ($conn, $id);

        header("location:index.php?p=manage-user");
        exit();
    } else {
        header("location:index.php?p=manage-user");
        exit();
    }
} else {
    header("location:index.php?p=manage-user");
    exit();
}
?>