<?php 
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    
    settype($id, 'int');

    if ($id != 0) {
        delete_posts ($conn, $id);

        header("location:index.php?p=manage-posts");
        exit();
    } else {
        header("location:index.php?p=manage-posts");
        exit();
    }
} else {
    header("location:index.php?p=manage-posts");
    exit();
}
?>