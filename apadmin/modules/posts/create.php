<?php
$errors = array();
$user_id = show_writer ($conn);

if (isset($_POST['create'])){
    if (empty($_POST['title'])){
        $errors[] = 'Please enter title';
    }
    if (empty($_POST['content'])){
        $errors[] = 'Please enter content';
    } 
    if (isset($_POST['images'])){
        if (!check_extension ($_FILES["images"]["name"])){
            $errors[] = "Please choose image file";
        }
    }
    
    if (empty($errors)){
        $data = array (
            'title' => resetXss ($_POST['title']),
            'slug' => resetXss ($_POST['slug']),
            'content' => resetXss ($_POST['content']),
            'images' => time().'_'.change_name_file ($_FILES["images"]["name"]),
            'user_id' =>resetXss ($_POST['user_id']),
            'source' => resetXss ($_POST['source']),
            'status' => (isset($_POST['status']) && $_POST['status'] == 'on') ? 1: 0,
            'tmp_name' => $_FILES["images"]["tmp_name"],
            'path_image' => '../uploads/posts/'.time()."_".$_FILES["images"]["name"]
        );

        create_posts ($conn, $data, $errors[]);
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create posts</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control convert_slug" name="title" data-slug="slug"  placeholder="Please enter title" <?php keep_value_input('title') ?> >
            </div>
            <div class="form-group">
                <label>Slug</label>
                <input type="text" class="form-control" name="slug"<?php keep_value_input ('slug') ?>>
            </div>
            <div class="form-group">
                <label>Content</label>
                <textarea name="content" id="" class="form-control" placeholder="Text here"><?php  keep_value_area ('content') ?></textarea>
                <script>
                    CKEDITOR.replace( 'content' );
                </script>
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control" name="images" >
            </div>
            <div class="form-group">
                <label>Source</label>
                <input type="text" class="form-control" name="source" >
            </div>
            <div class="form-group">
                <label>Copy writer</label>
                <select name="user_id" class="form-control">
                    <?php 
                        if (isset($_POST["user_id"])) {
                            recursive_user ($user_id , $_POST["user_id"]);
                        } else {
                            recursive_user ($user_id , 0);
                        }
                    ?>
                </select>
            </div>
            <input type="checkbox" name="status" class="bootstrap_switch" checked data-bootstrap-switch data-off-color="danger" data-on-color="success">
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="create" class="btn btn-primary">Create</button>
        </div>
    </form>
</div>