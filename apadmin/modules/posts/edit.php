
<?php

if (!isset($_GET["id"])){
    header("location:index.php?p=manage-posts");
    exit();
} 
$id = $_GET['id'];
if($id == 0){
    header("location:index.php?p=manage-posts");
    exit();
} else {
    settype($id, "int");
}

if (!check_id ($conn,'posts', $id)){
    header("location:index.php?p=manage-posts");
    exit();
}

$old = get_one_modules ($conn, $id, 'posts');
$user_data = list_modules ($conn, "user"); 

$errors = array();

if (isset($_POST['edit'])){
    if (empty($_POST['title'])){
        $errors[] = 'Please enter title';
    }
    if (empty($_POST['content'])){
        $errors[] = 'Please enter content';
    } 
    
    if (empty($errors)){
        $data = array (
            'title' => resetXss ($_POST['title']),
            'slug' => resetXss ($_POST['slug']),
            'content' => resetXss ($_POST['content']),
            'user_id' =>resetXss ($_POST['user_id']),
            'source' => resetXss ($_POST['source']),
            'status' => (isset($_POST['status']) && $_POST['status'] == 'on') ? 1: 0,
            'tmp_name' => $_FILES["images"]["tmp_name"],
            'path_image' => '../uploads/posts/'.time()."_".$_FILES["images"]["name"],
            'id' => $id
        );

        if (!empty($_FILES["images"]["name"])) {
            $data['images'] = time()."_".$_FILES["images"]["name"];
        }
        edit_posts ($conn, $data);
    }
}
?>
<?php if (!empty($errors)) {?>
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
    <?php foreach ($errors as $error) {
        echo "<li>".$error."</li>";
    }?>

</div>
<?php } ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Create posts</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="card-body">
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control convert_slug" name="title" data-slug="slug"  placeholder="Please enter title" <?php keep_value_input('title', $old['title']) ?> >
            </div>
            <div class="form-group">
                <label>Slug</label>
                <input type="text" class="form-control" name="slug"<?php keep_value_input ('slug', $old['slug']) ?>>
            </div>
            <div class="form-group">
                <label>Content</label>
                <textarea name="content" id="" class="form-control" placeholder="Text here"><?php  keep_value_area ('content', $old['content']) ?></textarea>
                <script>
                    CKEDITOR.replace( 'content' );
                </script>
            </div>
            <div class="form-group">
                <label>Old image</label>
                <img src="../uploads/posts/<?php echo $old['images'] ?>" width="100px" />
            </div>
            <div class="form-group">
                <label>Image</label>
                <input type="file" name="images" class="form-control">
            </div>
            <div class="form-group">
                <label>Source</label>
                <input type="text" class="form-control" name="source" <?php keep_value_input('source', $old["source"]) ?> >
            </div>
            <div class="form-group">
                <label>Copy writer</label>
                <select name="user_id" class="form-control">
                    <?php foreach ($user_data as $item) {
                        if ($item["id"] == $old["user_id"]) {?>
                        <option value="<?php echo $old["user_id"] ?>"><?php echo $item["fullname"] ?></option>
                    <?php }}?>
                </select>
            </div>
            <input type="checkbox" name="status" class="bootstrap_switch" data-bootstrap-switch data-off-color="danger" data-on-color="success" <?php 
                    if ($old["status"] == 1) {
                        echo 'checked';
                    }
                ?>>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" name="edit" class="btn btn-primary">Edit</button>
        </div>
    </form>
</div>