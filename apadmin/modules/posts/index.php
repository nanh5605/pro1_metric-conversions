<?php
$data = list_modules ($conn, "posts"); 
$user_data = list_modules ($conn, "user"); 
 ?>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">List of Posts</h3>
    </div>
    <div class="card-body">
        <table id="dataTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Copy writer</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $item) {
                    
                        
                    ?>
                <tr>
                    <td><?php echo $item['id'] ?></td>
                    <td><?php echo $item['title'] ?></td>
                    <td> <?php foreach ($user_data as $item_user){
                                    if ($item['user_id'] == $item_user["id"]){
                                        echo $item_user["fullname"];
                                    }} ?>
                    </td>
                    <td><?php echo ($item['status'] == 1) ? "On" : "Off" ?></td>
                    <td>
                        <a class="confirmDelete" href="index.php?p=delete-posts&id=<?php echo $item['id'] ?>">Delete</a> | 
                        <a href="index.php?p=edit-posts&id=<?php echo $item['id'] ?>">Edit</a>
                    </td>
                </tr>
                <?php }?>
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Copy writer</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>