
<?php
$all_unit = show_all_unit( $conn);
if (isset($_GET["id"])){
    $id = $_GET["id"];
    $pos = strpos($id, '-', 0);
    $pos2 = strpos($id, '-', $pos + 1);
    $unit1 = substr($id, 0, $pos);
    $unit2 = substr($id, $pos2 + 1);
}
?>
<section class="page-section">
    <div class="container pb-5">
        <div class="row">
            <div class="col-lg-8">
                <!-- Find the Units to Convert -->
                <div class="page_title pb-2 text-center">
                    <h3 style="color: #1D7C5A" >Convert <?php echo $unit1 ?> to <?php echo $unit2 ?></h3>
                </div>
                <div class="bg-light  px-5 py-3">
                    <table class="w-100 text-center">
                        <tbody class="">
                            <tr>
                                <td><b>From Unit:</b></td>
                                <?php foreach ($all_unit as $item) {
                                    if (strtolower( $item['name']) == $unit1){ ?>
                                <td ><input type="text" name="fromUnit" class="ucinput" autofocus="" /></td>
                                <td id="unitIn" data-unit="<?php echo $item['compare'] ?>"><?php echo ucfirst($item['name']) ?></td>
                                <?php }} ?>
                            </tr>
                            <tr>
                                <td><b>To Unit:</b></td>
                                <?php foreach ($all_unit as $item){
                                    if (strtolower($item['name']) == $unit2){ ?>
                                <td><input type="text" name="toUnit" class="ucinput" readonly /></td>
                                <td id="unitOut" data-unit="<?php echo $item['compare'] ?>" ><?php echo ucfirst($item['name']) ?></td>
                                <?php } }?>
                            </tr>
                        </tbody>
                    </table>
                    <div class="text-center">
                        <div class="btn-group mt-3 ">
                            <div id="clearUnit" ><a style="background-color:#59B896; border:none; color: white" class="btn btn-primary">Clear</a></div>
                        </div>
                    </div>
                    
                </div>
                <div class="unit_content mt-2 px-5 py-3">
                    <?php foreach ($all_unit as $item){
                        if (strtolower($item['name']) == $unit1){ ?>
                    <h5 style="color: #1D7C5A" ><?php echo ucfirst($item['name']) ?></h5>
                    <div class="content"><?php echo html_entity_decode( $item['history']) ?></div>
                    <?php } } ?>
                    <?php foreach ($all_unit as $item){
                        if (strtolower($item['name']) == $unit2){ ?>
                    <h5 style="color: #1D7C5A" ><?php echo ucfirst($item['name']) ?></h5>
                    <div class="content"><?php echo html_entity_decode( $item['history']) ?></div>
                    <?php } } ?>
                </div>
                <!--How to-->
                <div class="px-5 py-3">
                    <h4 style="color: #1D7C5A">How to convert <?php echo $unit1?> to <?php echo $unit2?></h4>
                    <?php foreach ($all_unit as $item1){
                        foreach ($all_unit as $item2){
                            if ( strtolower ($item1['name']) == $unit1 && strtolower( $item2['name']) == $unit2){?>
                    <p class="big-text">
                        1 <?php echo $item1['name'] ?> = <?php echo ($item1['compare'] / $item2['compare']).' '. $item2['name']?> <br>
                        1 <?php echo $item2['name'] ?> = <?php echo ($item2['compare'] / $item1['compare']).' '. $item1['name']?> 
                    </p>
                    <p class="big-text">
                        <b>Example: </b>convert 15 <?php echo strtolower($item1['name']) ?> to <?php echo strtolower($item2['name']) ?> <br>
                        15 <?php echo $item1['name'] ?> = <?php echo 15 *($item2['compare'] / $item1['compare']).' '. $item2['name']?> 
                    </p>
                    <?php  }}}  ?>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="list-group">
                    <button type="button" style="background-color:#59B896; border:none" class="list-group-item list-group-item-action active">All Converters</button>
                    <?php 
                        foreach ($cate_unit_data as $key => $item){
                        ?>
                            <button type="button" class="list-group-item list-group-item-action"><a style="color: #005E3C" href="index.php?p=all-converter&action=<?php echo strtolower( $item['name'])?>"><?php echo $item['name'] ?></a></button>

                        <?php  }?>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function (){
        $("input[name='fromUnit']").keyup(delay(function (){
            var valInput = $(this).val();
            var val1 = $("#unitIn").data('unit');
            var val2 = $("#unitOut").data('unit');
            var result = valInput * val1/ val2;
            $("input[name='toUnit']").val(result);
        }, 200));

        $("#clearUnit").click(function(){
            $("input[name='toUnit']").val('');
            $("input[name='fromUnit']").val('');
        });
    });
</script>
