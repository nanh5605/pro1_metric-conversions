
<section class="page-section">
    <div class="container pb-5">
        <div class="row">
            <div class="col-lg-8">
            <div class="pl-2 text-center">
                <h3 style="color: #1D7C5A" >
                <?php 
                    if (isset($_GET['action'])){
                        $action = $_GET['action'];
                        switch ($action){
                            case 'express-convert':
                                echo 'Unit Converter Express Version';
                                break;
                            case 'area':
                                echo 'Area Converter';
                                break;
                            case 'currency':
                                echo 'Currency Converter';
                                break;
                            case 'length':
                                echo 'Length Converter';
                                break;
                            case 'mass':
                                echo 'Mass Converter';
                                break;
                            case 'temperature':
                                echo 'Temperature Converter';
                                break;
                            case 'volume':
                                echo 'Volume Converter';
                                break;
                            default:
                                echo 'Unit Converter Express Version';
                        } 
                    }else {
                        echo 'Unit Converter Express Version';
                    }
                ?>
                </h3>
            </div>
            <div class="tab-content pl-5 bg-light bg-gradient rounded " id="nav-tabContent">
                <div class="tab-pane fade show active p-2" id="nav-home" role="tabpanel">
                <?php 
                if (isset($_GET['action'])){
                    $action = $_GET['action'];
                    switch ($action){
                        case 'area':
                            include 'area.php';
                            break;
                        case 'currency':
                            include 'currency.php';
                            break;
                        case 'length':
                            include 'length.php';
                            break;
                        case 'mass':
                            include 'mass.php';
                            break;
                        case 'temperature':
                            include 'temperature.php';
                            break;
                        case 'volume':
                            include 'volume.php';
                            break;
                        default:
                            header ("location:index.php");
                            exit();
                    } 
                }else {
                    header ("location:index.php");
                    exit();
                }
                ?>
                </div>
            </div>
                
            </div>
            <div class="col-lg-4">
                <div class="list-group">
                    <button type="button" class="list-group-item list-group-item-action active" style="background-color:#59B896; border:none" >All Converters</button>
                <?php  foreach ($cate_unit_data as $item){ ?>
                    <button type="button" class="list-group-item list-group-item-action"><a style="color: #005E3C" href="index.php?p=all-converter&action=<?php echo strtolower($item["name"]) ?>"><?php echo $item["name"] ?></a></button>
                <?php }?>
                </div>
            </div>
        </div>
    </div>
</section>