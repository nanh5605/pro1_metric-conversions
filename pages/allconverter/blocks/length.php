<?php 
function list_length ($conn) {
    $stmt = $conn->prepare("SELECT unit.id, unit.name, unit.compare, unit.history FROM unit, unit_category WHERE unit.unit_category_id = unit_category.id AND unit_category.name = 'length' ");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
$unit_name = list_length ($conn);
?>
<table class="w-100">
    <form name="calForm"></form>
    <tbody>
        <tr>
            <td><b>From:</b></td>
            <td><b>To:</b></td>
        </tr>
        <tr>
            <td><input type="text" name="fromVal" class="ucinput w-75" autofocus="" /></td>
            <td><input type="text" name="toVal" class="ucinput w-75" readonly="" /></td>
        </tr>
        <tr>
            <td style="padding-top: 8px;">
                <select name="calFrom" id="calFrom"  size="8" class="ucselect w-75">
                    <?php foreach ($unit_name as $item) {?>
                    <option id="<?php echo $item['name'] ?>Id1" data-unit="<?php echo $item['compare'] ?>" value="<?php echo $item['compare'] ?>" ><?php echo ucfirst($item['name']) ?></option>
                    <?php  }?>
                </select>
            </td>
            <td style="padding-top: 8px;">
                <select name="calTo" id="calTo" size="8" class="ucselect w-75">
                <?php foreach ($unit_name as $item) {?>
                    <option id="<?php echo $item['name'] ?>Id2" data-unit="<?php echo $item['compare'] ?>" value="<?php echo $item['compare'] ?>" ><?php echo ucfirst($item['name']) ?></option>
                    <?php  }?>
                </select>
            </td>
        </tr>
    </tbody>
</table>
<?php if (isset($_GET['action'])) { ?>
<div class="mt-4 py-3">
    <h4 style="color: #1D7C5A">Popular length unit conversions</h4>
    <table>
        <tr>
            <td>
                <ul>
    <?php 
    foreach ($unit_name as $key1 => $item1){
    foreach ($unit_name as $key2 => $item2){
    if ($key1 != $key2){
    echo strtolower('<li><a style="color: #005E3C;" href="index.php?p=all-converter&action=length&id='.$item1["name"].'-to-'.$item2["name"].'">'.$item1['name'].' to '.$item2["name"].'</a></li>');
    }}}?>
                </ul>
            </td>
        </tr>
    </table>
</div>
<?php } ?>