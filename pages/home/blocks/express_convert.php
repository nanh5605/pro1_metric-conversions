<?php
    $cate_unit_data = list_modules_user ($conn);
?>
<section class="page-section">
    <div class="container pt-3 pb-5">
        <div class="row">
            <div class="col-lg-8">
                <div class="text-center page_title pb-2">
                    <h3 style="color: #1D7C5A" >Unit Converter Express Version </h3>
                </div>
                <div class="tab-content " id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel">
                    <form action="POST" >
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <?php 
                                    foreach ($cate_unit_data as $key => $item) {
                                        
                                        if ($key == 0){ ?>
                                            <button style="color:#45A482;" class="nav-link title-color active" id="nav-<?php echo $item['name'] ?>-tab" data-bs-toggle="tab" data-bs-target="#nav-<?php echo $item['name'] ?>" type="button" role="tab" aria-controls="nav-<?php echo $item['name']  ?>" aria-selected="false" name="<?php echo $item['name'] ?>"><b><?php echo $item['name'] ?></b></button>
                                        <?php } else {?> 
                                            <button  style="color: #45A482;" class="nav-link title-color " id="nav-<?php echo $item['name'] ?>-tab" data-bs-toggle="tab" data-bs-target="#nav-<?php echo $item['name'] ?>" type="button" role="tab" aria-controls="nav-<?php echo $item['name'] ?>" aria-selected="false" name="<?php echo $item['name'] ?>" ><b><?php echo $item['name'] ?></b></button>

                                    <?php }}?>
                                    
                                </div>
                            </nav>
                        </form>
                        <div class="tab-content pl-5 pt-3 bg-light bg-gradient rounded pb-4" id="nav-tabContent">
                            <?php 
                            foreach ($cate_unit_data as $key => $item){
                                if ($key == 0){
                            ?>
                                <div class="tab-pane fade show active" id="nav-<?php echo $item['name'] ?>" role="tabpanel" aria-labelledby="nav-<?php echo $item['name'] ?>-tab"><?php include 'pages/allconverter/blocks/'.strtolower( $item['name']).'.php' ?></div>
                                <?php }else {?>
                                    <div class="tab-pane fade" id="nav-<?php echo $item['name'] ?>" role="tabpanel" aria-labelledby="nav-<?php echo $item['name'] ?>-tab"><?php include 'pages/allconverter/blocks/'.strtolower( $item['name']).'.php' ?></div>
                            <?php } }?>
                        </div>
                    </div>
                </div>
                <!-- Find the Units to Convert -->
                <div class="bg-light mt-5  px-5 py-3">
                    <div class="page_title pb-2">
                        <h4 style="color: #1D7C5A" >Find the Units to Convert </h4>
                    </div>
                    <table class="w-100">
                        <tbody class="">
                            <tr>
                                <td><b>From Unit:</b></td>
                                <td><b>To Unit:</b></td>
                            </tr>
                            <tr>
                                <td><input type="text" id="findFrom" name="findFrom" class="ucinput w-75" autofocus="" placeholder="e.g.kilogram" /></td>
                                <td><input type="text" id="findTo" name="findTo" class="ucinput w-75" placeholder="e.g.pound" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="result"></div>
                </div>
                <!-- Unit Converters - Full Versions -->
                <div class="mt-4 px-4 py-3">
                    <h4 style="color: #1D7C5A">Unit Converters - Full Versions</h4>
                    <table>
                        <tr>
                            <td>
                                <ul>
                                    <?php $cate_data = show_data_cate($conn);
                                    foreach ($cate_data as $cate_item){
                                        $unit_data = show_data_unit($conn, $cate_item['id']);
                                        foreach ($unit_data as $key1 => $item1){
                                            foreach ($unit_data as $key2 => $item2){
                                                if ($key1 != $key2){
                                                    echo '<li><a style="color: #005E3C;" href="index.php?p=all-converter&action='.$cate_item['name'].'&id='.strtolower($item1["name"]).'-to-'.strtolower($item2["name"]).'">'.$item1['name'].' to '.$item2["name"].'</a></li>';
                                                }
                                            }
                                        }
                                    } ?>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <?php $cate_data = show_data_cate($conn);
                                    foreach ($cate_data as $cate_item){
                                        $unit_data = show_data_unit($conn, $cate_item['id']);
                                        foreach ($unit_data as $key1 => $item1){
                                            foreach ($unit_data as $key2 => $item2){
                                                if ($key1 != $key2){
                                                    echo '<li><a style="color: #005E3C;" href="index.php?p=all-converter&action='.$cate_item['name'].'&id='.strtolower($item1["name"]).'-to-'.strtolower($item2["name"]).'">'.$item1['name'].' to '.$item2["name"].'</a></li>';
                                                }
                                            }
                                        }
                                    } ?>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="list-group">
                    <button type="button" style="background-color:#59B896; border:none" class="list-group-item list-group-item-action active">All Converters</button>
                    <?php 
                        foreach ($cate_unit_data as $key => $item){
                        ?>
                            <button type="button" class="list-group-item list-group-item-action"><a style="color: #005E3C" href="index.php?p=all-converter&action=<?php echo strtolower( $item['name'])?>"><?php echo $item['name'] ?></a></button>

                        <?php  }?>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" >
    $(document).ready(function (){
        $("input[name='findFrom']").keyup(delay(function (){
            var name = $(this).val();
            if ($(this).val().length != 0){
                $.ajax({
                type: "POST",
                url: "ajax/search-unit.php",
                data: {"name" : name},
                dataType: "html",
                success: function (response) {
                    $('.result').html(response);
                }
            });
            } else {
                $('.result').html('');
            }
            
        }, 200));
        $("input[name='fromVal']").keyup(delay(function (){
            var valInput = $(this).val();
            function displayVals (){
                var val1 = $("#nav-Volume select[name='calFrom']").val();
                var val2 = $("#nav-Volume select[name='calTo']").val();
                var pheptinh = valInput * val1 / val2 ;
                if (isNaN(pheptinh) == true){
                    $("input[name='toVal']").val("Please provide a valid number!");
                } else {
                    $("input[name='toVal']").val(pheptinh);
                }
            }
            $( "#nav-Volume select[name='calFrom'], #nav-Volume select[name='calTo']" ).change( displayVals );
            displayVals ();
        }, 100));
        $("input[name='fromVal']").keyup(delay(function (){
            var valInput = $(this).val();
            function displayVals (){
                var val1 = $("#nav-Currency select[name='calFrom']").val();
                var val2 = $("#nav-Currency select[name='calTo']").val();
                var pheptinh = valInput * val1 / val2 ;
                if (isNaN(pheptinh) == true){
                    $("input[name='toVal']").val("Please provide a valid number!");
                } else {
                    $("input[name='toVal']").val(pheptinh);
                }
            }
            $( "#nav-Currency select[name='calFrom'], #nav-Currency select[name='calTo']" ).change( displayVals );
            displayVals ();
        }, 100));
        $("input[name='fromVal']").keyup(delay(function (){
            var valInput = $(this).val();
            function displayVals (){
                var val1 = $("#nav-Temperature select[name='calFrom']").val();
                var val2 = $("#nav-Temperature select[name='calTo']").val();
                var pheptinh = valInput * val1 / val2 ;
                if (isNaN(pheptinh) == true){
                    $("input[name='toVal']").val("Please provide a valid number!");
                } else {
                    $("input[name='toVal']").val(pheptinh);
                }
            }
            $( "#nav-Temperature select[name='calFrom'], #nav-Temperature select[name='calTo']" ).change( displayVals );
            displayVals ();
        }, 100));
        $("input[name='fromVal']").keyup(delay(function (){
            var valInput = $(this).val();
            function displayVals (){
                var val1 = $("#nav-Area select[name='calFrom']").val();
                var val2 = $("#nav-Area select[name='calTo']").val();
                var pheptinh = valInput * val1 / val2 ;
                if (isNaN(pheptinh) == true){
                    $("input[name='toVal']").val("Please provide a valid number!");
                } else {
                    $("input[name='toVal']").val(pheptinh);
                }
            }
            $( "#nav-Area select[name='calFrom'], #nav-Area select[name='calTo']" ).change( displayVals );
            displayVals ();
        }, 100));
        $("input[name='fromVal']").keyup(delay(function (){
            var valInput = $(this).val();
            function displayVals (){
                var val1 = $("#nav-Mass select[name='calFrom']").val();
                var val2 = $("#nav-Mass select[name='calTo']").val();
                var pheptinh = valInput * val1 / val2 ;
                if (isNaN(pheptinh) == true){
                    $("input[name='toVal']").val("Please provide a valid number!");
                } else {
                    $("input[name='toVal']").val(pheptinh);
                }
            }
            $( "#nav-Mass select[name='calFrom'], #nav-Mass select[name='calTo']" ).change( displayVals );
            displayVals ();
        }, 100));
    });
</script>