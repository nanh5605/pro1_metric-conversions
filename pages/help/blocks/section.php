<?php 
$faqs = list_modules_user ($conn, 'faqs');
?>
<section class="page-section ">
    <div class="container pb-5">
        <h5 class="text-center" style="color: #1D7C5A"><b>
            Can't find the answer you're looking for? <br />
            We've shared some of most frequently asked questions to help you out!</b>
        </h5>
        <!-- <ul class="nav nav-tabs " id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">FAQs</button>
            </li>
        </ul> -->
        <div class="accordion accordion-flush pt-3" id="accordionFlushExample">
                    <?php foreach ($faqs as $key => $item) {?>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne<?php echo $item['id'] ?>">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne<?php echo $item['id'] ?>" aria-expanded="false" aria-controls="flush-collapseOne<?php echo $item['id'] ?>">
                                    <?php echo $item["question"] ?> 
                                </button>
                            </h2>
                            <div id="flush-collapseOne<?php echo $item['id'] ?>" class="accordion-collapse collapse" aria-labelledby="flush-headingOne<?php echo $item['id'] ?>" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <?php echo $item["answer"] ?>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
        <!-- <div class="tab-content " id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                
            </div>
        </div> -->
    </div>
</section>