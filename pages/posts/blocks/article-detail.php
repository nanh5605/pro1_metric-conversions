<?php
if (!isset($_GET["id"])){
    header("location:index.php?p=posts&action=article");
    exit();
} 
$id = $_GET['id'];

if ($id == 0){
    header("location:index.php?p=posts&action=article");
    exit();
} else {
    settype($id, 'int');
}

$article = show_article ($conn, $id);

?>
<section class="blog-details pt-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <?php foreach ($article as $key => $item) {?>
                <article class="post">
                    <div class="post-image">
                        <img class="img-fluid w-100" src="uploads/posts/<?php echo $item['images'] ?>" alt="post-image" />
                    </div>
                    <!-- Post Content -->
                    <div class="post-content pt-3">
                    <h3 style="color: #45A482"><?php echo $item['title'] ?></h3>
                        <hr class="w-25">
                        <div class="content" >
                        <?php  
                           echo html_entity_decode ($item['content'])
                         ?> 
                        </div>
                        
                         <div class="text-right" ><span class="text-muted">Source: <?php echo $item['source'] ?></span></div>
                        
                        <!-- post share -->
                        <ul class="post-content-share list-inline">
                            <li class="list-inline-item">
                                <a href="#">
                                    <i class="tf-ion-social-twitter"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <i class="tf-ion-social-linkedin"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <i class="tf-ion-social-facebook"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <i class="tf-ion-social-skype"></i>
                                </a>
                            </li>
                        </ul>
                        <div class=" pr-4 pb-3 text-right"><a style="background-color:#59B896; border:none;" href="index.php?p=posts&action=article" class="btn btn-primary">Back</a></div>
                    </div>
                </article>
                <?php }?>
            </div>
            <div class="col-lg-4 ">
                <!-- sidebar -->
                <aside class="sidebar">
                    
                    <div class="widget-post widget p-3 bg-light rounded">
                        <h2 style="color:#31906E;">Latest Post</h2>
                        <!-- latest post -->
                        <ul class="widget-post-list">
                        <?php foreach (latest_post ($conn, $id) as $item) {?>
                            <li class="widget-post-list-item">
                                <div class="row">
                                    <div class="widget-post-image col-lg-4 col-md-4">
                                        <a class="text-decoration-none" href="index.php?p=posts&action=article-detail&id=<?php echo $item['id'] ?>">
                                            <img class="" src="uploads/posts/<?php echo $item['images'] ?>" alt="post-img" />
                                        </a>
                                    </div>
                                    <div class="widget-post-content col-lg-7 col-md-7 p-0">
                                        <a class="text-decoration-none" href="index.php?p=posts&action=article-detail&id=<?php echo $item['id'] ?>">
                                            <h5 ><?php echo $item['title'] ?></h5>
                                        </a>
                                        <h6>Source: <?php echo $item['source'] ?></h6>
                                    </div>
                                </div>
                            </li>
                            <?php }?>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</section>



