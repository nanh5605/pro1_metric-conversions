<?php $post = list_post ($conn, 1) ?>
<?php 
if (isset($_GET['action'])){
    $action = $_GET['action'];
    if ($action == 'article-detail'){
        include 'pages/posts/blocks/article-detail.php';
    } else {
        header ("location:index.php");
        exit ();
    }
} else {
?>
<style>
.posts hr:last-child{
    display:none;
}
</style>
<section class="posts pt-1 pb-5">
	<div class="container">
		<div class="row">
			<!-- single blog post -->
            <?php foreach ($post as $item) { ?>
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12 ">
                        <a href=""><img class ="w-100 " src="uploads/posts/<?php echo $item['images'] ?>" alt="<?php echo $item['slug'] ?>"></a>
                    </div>
                    <div class="col-md-9 col-sm-8 col-xs-12 ">
                        <div class=" col-lg-12 pt-3">
                            <h5 class="card-title"><a style="color: #45A482" href="#"><?php echo html_entity_decode($item['title']) ?></a></h5>
                        </div>
                        <div class="content col-lg-12 " style="color: #00401E"><?php readmore (html_entity_decode ($item['content'])) ?></div>
                        
                    </div>
                    <div class=" pr-4 pb-3 text-right"><a style="background-color:#59B896; border:none;" href="index.php?p=article&action=article-detail&id=<?php echo $item['id'] ?>" class="btn btn-primary">Read more</a></div>
                </div>
                <hr>
            <?php }?>
			<!-- /single blog post -->
		</div>
	</div>
</section>
<?php }?>