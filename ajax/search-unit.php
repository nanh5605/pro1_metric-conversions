<?php
include '../core/config.php';
include '../core/connect.php';

function show_data_cate($conn){
    $stmt = $conn->prepare("SELECT * FROM unit_category");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function show_data_unit($conn, $id, $name){
    $stmt = $conn->prepare("SELECT * FROM unit WHERE unit_category_id = :id AND name LIKE '%$name%'");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
function show_data_unit2($conn, $id, $name){
    $stmt = $conn->prepare("SELECT * FROM unit WHERE unit_category_id = :id");
    $stmt->bindParam(":id", $id, PDO::PARAM_INT);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}
$name = $_POST['name'];
$cate_data = show_data_cate($conn);

$xhtml = '<ul>';
foreach ($cate_data as $cate_item){
    $unit_data = show_data_unit($conn, $cate_item['id'], $name);
    $unit_data2 = show_data_unit2($conn,  $cate_item['id'], $name);
    foreach ($unit_data as $key1 => $item1){
        foreach ($unit_data2 as $key2 => $item2){
            if ($item1['name'] != $item2['name'] ){
                $xhtml .= '<li><a style="color: #005E3C;" href="index.php?p=all-converter&action='. $cate_item['name'].'&id='.strtolower($item1["name"]).'-to-'.strtolower($item2["name"]).'">'.$item1['name'].' to '.$item2['name'];
            }
        }
    }
} 
$xhtml .= '</ul>';

echo $xhtml;
?>


