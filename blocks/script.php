<script src="public/assets/plugins/jquery/jquery.min.js"></script>
<!-- Google Map -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu5nZKbeK-WHQ70oqOWo-_4VmwOwKP9YQ"></script> -->
<script  src="public/assets/plugins/google-map/gmap.js"></script>

<!-- Form Validation -->
<script src="public/assets/plugins/form-validation/jquery.form.js"></script> 
<script src="public/assets/plugins/form-validation/jquery.validate.min.js"></script>

<!-- Bootstrap4 -->
<script src="public/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Parallax -->
<script src="public/assets/plugins/parallax/jquery.parallax-1.1.3.js"></script> 
<!-- lightbox -->
<script src="public/assets/plugins/lightbox2/dist/js/lightbox.min.js"></script>
<!-- Owl Carousel -->
<script src="public/assets/plugins/slick/slick.min.js"></script>
<!-- filter -->
<script src="public/assets/plugins/filterizr/jquery.filterizr.min.js"></script>
<!-- Smooth Scroll js -->
<script src="public/assets/plugins/smooth-scroll/smooth-scroll.min.js"></script>

<!-- Custom js -->
<script src="public/assets/js/script.js"></script>
<!-- Bootstrap -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>