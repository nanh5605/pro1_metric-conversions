<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container px-5">
        <a href="index.php"><img class="rounded-circle" style="width: 80px" src="uploads/logo_unit_convert.png" alt="logo"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
        </button>
        <div class="text-right">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 ">
                    <li class="nav-item">
                        <a style="color: #005E3C" class="nav-link active" aria-current="page" href="index.php">All Converters</a>
                    </li>
                    <li class="nav-item px-4">
                        <a style="color: #005E3C" class="nav-link" href="index.php?p=article">Article</a>
                    </li>
                    <li class="nav-item">
                        <a style="color: #005E3C" class="nav-link" href="index.php?p=help">Help</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>