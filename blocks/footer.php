<?php
    $cate_unit_data = list_modules_user ($conn);
    $post_data = list_modules_user ($conn, 'posts');
?>
<footer id="footer" class="bg-one">
    <div class="top-footer py-5 text-center ">
        <div class="container">
            <div class="row ml-5">
                <!-- End of .col-sm-4 -->
                <div class="col-sm-4 col-md-4 col-lg-4 ">
                    <ul>
                        <li><h3>About</h3></li>
                        <p>The main purpose of this website is to provide a comprehensive collection of online unit converters for the ease of public use. This site was launched in 2021.</p>
                    </ul>
                </div>

                <div class="col-sm-3 col-md-3 col-lg-3 ">
                    <ul>
                        <li><h3>Quick Links</h3></li>
                        <?php foreach ($cate_unit_data as $item) {?>
                        <li><a class="text-decoration-none" href="index.php?p=all-converter&action=<?php echo strtolower( $item['name'])?>"><?php echo $item['name'] ?></a></li>
                        <?php }?>
                    </ul>
                </div>
                <!-- End of .col-sm-4 -->

                
                <!-- End of .col-sm-4 -->

                <div class="col-sm-4 col-md-4 col-lg-4 ">
                    <ul class="post-content-share list-inline">
                    <h3>Connect with us Socially</h3>
                        <li class="list-inline-item">
                            <a href="#">
                            <i class="tf-ion-social-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                            <i class="tf-ion-social-linkedin"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                            <i class="tf-ion-social-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">
                            <i class="tf-ion-social-skype"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End of .col-sm-4 -->
            </div>
        </div>
        <!-- end container -->
    </div>
    <div class="footer-bottom py-2">
        <h5>Copyright &copy; Metric Conversion 2021</h5>
    </div>
</footer>
